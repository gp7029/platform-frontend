import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import MainIdeasDisplay from './components/ideas/main';
import Account from './components/user-profile/common/account'

// This file will have router declaration componets
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <MainIdeasDisplay/>
        </Route>
        <Route path="/myaccount">
          <Account/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
