import React, {Component} from 'react';

const Modal = (props) => {
    return (
        <div className="modal fade" id={props.modal_id} >
            <div className={`modal-dialog ${props.isLarge ? "modal-lg" : ""} ${props.isCentered ? "modal-dialog-centered" : ""}`} role="document">
                <div className="modal-content">
                    {props.children}
                </div>
            </div>
        </div>
    )
}

const Header = (props) =>{
    return(
        <div className="modal-header">
            {props.children}
        </div>
    )
}

const Body = (props) => {
    return (
        <div className="modal-body">
            {props.children}
        </div>
    )
}

const Footer = (props) => {
    return (
        <div className="modal-footer">
            {props.children}
        </div>
    )
}

Modal.Header = Header;
Modal.Body = Body;
Modal.Footer = Footer;

export default Modal;