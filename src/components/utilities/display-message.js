import React, {forwardRef, Component} from 'react';

// const DisplayError = forwardRef((props, ref)=>{
//     return (<div>
//         <span className="display-error" ref={ref} ></span>
//     </div>)
// });

class DisplayMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            message: '',
            message_type: ''
        }
        this.handleDismiss = this.handleDismiss.bind(this);
        this.setMessageState = this.setMessageState.bind(this);
    }
    
    handleDismiss(){
        this.setState({message: '', message_type:''});
    }

    setMessageState(message, message_type){
        this.setState({message: message, message_type: message_type});
    }

    render(){
        return(
            <div>
                {
                    (this.state.message) 
                    ?
                    <div>
                        <i className="fas fa-times" onClick={this.handleDismiss}></i>
                        <span className={`display-message-${this.state.message_type}`} >
                            {this.state.message}
                        </span>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

export default DisplayMessage;