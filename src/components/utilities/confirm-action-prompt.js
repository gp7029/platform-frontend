import React from 'react';
import Modal from './modal';

const ConfirmActionPrompt = (props) => (
    <Modal modal_id={props.modal_id} isLarge={false} isCentered={true} >
        <Modal.Body>
            {props.promptText}
        </Modal.Body>
        <Modal.Footer>
            <button className="btn btn-primary" onClick={props.handleConfirm}>Confirm</button>
            <button className="btn btn-primary" data-dismiss="modal">Cancel</button>
        </Modal.Footer>
    </Modal>
);

export default ConfirmActionPrompt;