import React, {Component} from 'react';
import UserIdeas from '../inventor/user-ideas';
import {Switch, Route, Link, useRouteMatch} from 'react-router-dom';

class Account extends Component {
    constructor(){
        super();
        this.state = {}
    }
    render(){
        return (
            <div>
                <Banner/>
                <Settings/>
            </div>
        )
    }
}

const Banner = (props) => {
    return (
        <div className="banner-container">
            <div className="banner-user-name-container">
                <span className="banner-user-name">Full User Name</span>
            </div>
        </div>
    )
}

const Settings = (props) => {
    let { path, url } = useRouteMatch();
    console.log({path})
    return(
        <div className="account-settings-container">
            <div className="account-settings-tabs">
                <ul>
                    <li><span className="account-settings-li"><Link to={`${url}/ideas`}>My Ideas</Link></span></li>
                    <li><span className="account-settings-li"><Link to={`${url}/messages`}>My Messages</Link></span></li>
                    <li><span className="account-settings-li"><Link to={`${url}/settings`}>Profile Settings</Link></span></li>
                    <li><span className="account-settings-li"><Link to={`${url}/billing`}>Billing Info</Link></span></li>
                </ul>
            </div>
            <Switch>
                <Route path="/myaccount/ideas">
                    <UserIdeas/>
                </Route>
                <Route path="/myaccount/messages">

                </Route>
                <Route path="/myaccount/settings">

                </Route>
                <Route path="/myaccount/billing">

                </Route>
            </Switch>
        </div>
    )
}

export default Account;