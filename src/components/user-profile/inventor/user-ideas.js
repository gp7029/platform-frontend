import React, {Component} from 'react';
import IdeaCard from '../../ideas/idea-card';
import SubmitIdea from './submit-idea';
import {getActiveLogin, api_calls, isValidResponse} from '../../../utilities'

class UserIdeas extends Component {
    constructor(props){
        super(props);
        this.state = {
            tabs_to_show: ["Published", "Drafts", "Favorites", "Liked"],
            ideas_container: [],
            filtered_ideas: [],
            favorite_fetched: [],
            liked_fetched: [],
            disliked_ideas: [],
            selected_ideas_tab: 'Published'
        }
        this.handleNewIdeaState = this.handleNewIdeaState.bind(this);
        this.fetchDataByTab = this.fetchDataByTab.bind(this);
        this.fetchUserIdeas = this.fetchUserIdeas.bind(this);
        this.handleDeleteIdea = this.handleDeleteIdea.bind(this);
    }

    componentDidMount(){
        this.fetchUserIdeas();
    }

    async fetchUserIdeas(){
        const user_id = getActiveLogin().userid;
        const response = await api_calls.GET(`/users/${user_id}/ideas`);
        if( isValidResponse(response) ) {
            this.setState((state)=>{
                const initial_ideas = [...state.ideas_container, ...response.data.payload]
                return {
                    ideas_container: initial_ideas,
                    filtered_ideas: ( initial_ideas || [] ).filter((idea)=> idea.submissionType==='publish')
                }
            })
        }
    }

    handleNewIdeaState( new_idea = {} ) {
        console.log("new idea", new_idea)
        this.setState((state) => {
            const updated_container = [...state.ideas_container, new_idea]
            return {
                ideas_container : updated_container,
                filtered_ideas : updated_container
            }
        })
    }

    async fetchDataByTab(tab_name){
        const user_id = getActiveLogin().userid;
        this.setState({
            selected_ideas_tab: tab_name
        });

        if (tab_name === "Favorites") {
            if ( !(this.state.favorite_fetched || []).length ){
                const response = await api_calls.GET(`/users/${user_id}/ideas`, {criteria: 'favorites'})
                if (isValidResponse(response)){
                    this.setState({
                        favorite_fetched: response.data.payload
                    });
                }
            }
        }
        if (tab_name === "Liked") {
            if ( !(this.state.liked_fetched || []).length ){
                const response = await api_calls.GET(`/users/${user_id}/ideas`, {criteria: 'likes'})
                console.log({response})
                if (isValidResponse(response)){
                    this.setState({
                        liked_fetched: response.data.payload
                    });
                }
            }
        }
    }

    async handleDeleteIdea(idea_id){
        const user_id = getActiveLogin().userid;
        if (user_id && idea_id){
            const response = await api_calls.DELETE(`/users/${user_id}/ideas/${idea_id}`)
            if(isValidResponse(response)){
                const ideas_container = (this.state.ideas_container || []).filter(idea_object => idea_object._id !== idea_id ) ;
                this.setState({
                    ideas_container
                })
            }
        }
    }
    
    render(){
        const current_tab = this.state.selected_ideas_tab;
        const user_id = getActiveLogin().userid;
        let filtered_ideas = [];
        const ideas_container = this.state.ideas_container || [];

        switch (current_tab) {
            case "Published":
                filtered_ideas = ( ideas_container || [] ).filter((idea)=> idea.submissionType==='publish' )
                break;
            case "Drafts":
                filtered_ideas = ( ideas_container || [] ).filter((idea)=> idea.submissionType==='draft')
                break;
            case "Favorites":
                filtered_ideas = this.state.favorite_fetched;
                break;
            case "Liked":
                filtered_ideas = this.state.liked_fetched;
                break;
            default:
                filtered_ideas = ideas_container;
                break;
        }
        return (
            <div className="inventor-ideas-container">
                <div className="container height-100">
                    <div className="row height-100">

                        <div className="col-2" >
                            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#submit-idea">
                                Submit New Idea
                            </button>
                            
                            <SubmitIdea handleNewIdeaState={this.handleNewIdeaState} modal_id="submit-idea" />
                            
                            <ul className="draft-published">
                                {["Published", "Drafts", "Favorites", "Liked"].map((tab_name, i)=>{
                                    return <li className={(current_tab===tab_name) ? "idea-tab-active": ""} key={`idea_${tab_name}`}
                                                    onClick={ ()=> this.fetchDataByTab(tab_name) }>
                                                {tab_name}
                                            </li>
                                })}
                            </ul>
                        </div>

                        <div className="col-10">
                        {
                            filtered_ideas.map((idea)=>{
                                return <IdeaCard key={`${this.state.selected_ideas_tab}_${idea._id}`} {...idea} isDeletable={user_id===idea.owner} 
                                        handleDeleteIdea={this.handleDeleteIdea} />
                            })
                        }
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default UserIdeas;