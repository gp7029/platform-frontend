import React, {Component} from 'react';
import Modal from '../../utilities/modal';
import {getActiveLogin, api_calls, isValidResponse, messageHandler} from '../../../utilities'
import DisplayMessage from '../../utilities/display-message'

const Title = (props) => {
    return(
        <div>
            <input className="input-box" name='title' onChange={(e)=>{props.handleInputs(e.target)}} 
                value={props.value} placeholder='Title of your idea' />
        </div>
    )
}

const Body = (props) => {
    return(
        <div>
            <textarea rows="8" className="text-area-box" name='description' onChange={(e)=>{props.handleInputs(e.target)}}
                value={props.value} placeholder="Describe your idea in detail here." />
        </div>
    )
}

class SubmitIdea extends Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            description: ''
        }
        this.message_ref = React.createRef();
        this.handleInputs = this.handleInputs.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputs(e){
        this.setState({
            [e.name]: e.value
        });
    }

    async handleSubmit(e){
        const message_node = this.message_ref.current;
        const userid = getActiveLogin().userid;
        const submission_type = e.target.name;
        const {title, description} = this.state;

        if (title && description) {
            const response = await api_calls.POST(`/users/${userid}/ideas`, { title, description, submission_type })
            
            if (isValidResponse(response)) { 
                this.props.handleNewIdeaState({ _id: response.data.payload.object_id, title, description, submissionType: submission_type, owner: userid });
                messageHandler(message_node, response.data.message, 'success');
                
                this.setState({
                    title: '',
                    description: ''
                })

            } else {
                messageHandler(message_node, response.data.message, 'error');
            }

        }
    }

    render(){
        return(
            <Modal modal_id={this.props.modal_id} isLarge={true} isCentered={false} >
                <Modal.Body>
                    <div>
                        <Title value={this.state.title} handleInputs={this.handleInputs}/>
                        <Body value= {this.state.description} handleInputs={this.handleInputs} /> 
                    </div>
                    <DisplayMessage ref={this.message_ref} />
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-primary" name='draft' onClick={this.handleSubmit} >Save Draft</button>
                    <button className="btn btn-primary" name='publish' onClick={this.handleSubmit} >Publish</button>
                    <button className="btn btn-primary" data-dismiss="modal">Cancel</button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default SubmitIdea;