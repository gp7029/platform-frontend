import React, {Component} from 'react';
import {api_calls, isValidResponse, getActiveLogin} from '../../utilities'
import ConfirmActionPrompt from '../utilities/confirm-action-prompt';
class IdeaCard extends Component {
    constructor(props){
        super(props);
        this.feedback_user_mapping = {
            favorites: "favoriteBy",
            likes: "likedBy",
            dislikes: "dislikedBy"
        }
        this.state = {
            favorites: props.favorites || 0,
            likes: props.likes || 0,
            dislikes: props.dislikes || 0,
            views: props.views || 0,
            favoriteBy: props.favoriteBy || [],
            likedBy: props.likedBy || [],
            dislikedBy: props.dislikedBy || []
        }
    }

    async handleFeedback(feedback_type) {
        const idea_id = this.props._id;
        const user_id = getActiveLogin().userid;
        if (user_id === this.props.owner) {
            return;
        }
        if (idea_id && feedback_type) {
            const payload = {
                user_id: user_id
            }

            const feedbackBy_field = this.feedback_user_mapping[feedback_type]            
            const ifUserInFeedbackByField = this.ifUserInFeedbackByField(feedbackBy_field);
            const restCall =  ifUserInFeedbackByField ? api_calls.DELETE : api_calls.POST;

            const response = await restCall(`/ideas/${idea_id}/${feedback_type}`, payload)

            if (isValidResponse(response) && response.data.payload.ok == 1) {
                this.setState((state)=>{
                    return {
                        [feedback_type]: state[feedback_type] + (ifUserInFeedbackByField ? -1 : 1),
                        [feedbackBy_field]: (ifUserInFeedbackByField
                            ? (state[feedbackBy_field] || []).filter(
                                (feedback_user)=>feedback_user!==user_id
                            )
                            : [user_id, ...state[feedbackBy_field]]
                            
                            )
                    }
                })
            }
        }
    }

    componentWillUnmount(){
        console.log('unmounting')
    }

    ifUserInFeedbackByField(feedbackBy_field) {
        console.log(this.state)
        const user_id = getActiveLogin().userid;
        return (this.state[feedbackBy_field].indexOf(user_id) || []) > -1;
    }

    render() {
        const idea_id = this.props._id;
        return (
            <div key={`eachidea_${idea_id}`} className="idea-container">
                <div>
                    <span className="idea-text">{this.props.title}</span>
                    {
                        this.props.isDeletable 
                        ? <span className="float-right" data-toggle="modal" data-target={`#delete-idea-${idea_id}`} >
                            <i className="fas fa-times icon-touchup" ></i>
                            <ConfirmActionPrompt modal_id={`delete-idea-${idea_id}`} 
                                promptText="This action will permanently delete this idea. Are you sure?" 
                                handleConfirm={()=>this.props.handleDeleteIdea(idea_id)}
                                />
                          </span> 
                        : null
                    }
                </div>
                <p className="idea-description">
                    {this.props.description}
                </p>
                <div className="idea-actions">
                    <span className="icon-label">
                        <i className={`fa${this.ifUserInFeedbackByField('favoriteBy') ? 's':'r'} fa-star icon-touchup icon-star`} 
                            title="Favorite" onClick={this.handleFeedback.bind(this, ['favorites'])} >
                        </i>
                        {this.state.favorites || 0}
                    </span>    
                    
                    <span className="icon-label">
                        <i className={`fa${this.ifUserInFeedbackByField('likedBy') ? 's':'r'} fa-thumbs-up icon-touchup icon-thumbs-up`} 
                            title="Like" onClick={this.handleFeedback.bind(this, ['likes'])} >
                        </i>
                        {this.state.likes || 0}
                    </span>
                    
                    <span className="icon-label">
                        <i className={`fa${this.ifUserInFeedbackByField('dislikedBy') ? 's':'r'} fa-thumbs-down icon-touchup icon-thumbs-down`} 
                            title="Dislike" onClick={this.handleFeedback.bind(this, ['dislikes'])} >
                        </i>
                        {this.state.likes || 0}
                    </span>
                    
                    <div className="userinfo float-right">
                        <span>Posted by <u className="username-display">{this.props.owner}</u> 10 hours ago</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default IdeaCard;