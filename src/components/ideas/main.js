import React, { Component } from 'react';
import IdeaCard from './idea-card';
import { Link } from 'react-router-dom';
import { api_calls, isValidResponse } from '../../utilities';

class MainIdeasDisplay extends Component {
    constructor(props){
        super(props);
        this.state = {
            ideas_container : []
        }
    }

    componentDidMount(){
        this.fetchIdeas();
    }
    
    async fetchIdeas(){
        const response = await api_calls.GET('/ideas');
        if (isValidResponse(response)) {
            this.setState((state)=>{
                return {
                    ideas_container: [...state.ideas_container, ...response.data.payload]
                }
            })
        }
    }

    render(){
        console.log(this.state.ideas_container);
        return (
            <div>
                <div className="ideas-top-panel">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-3"></div>
                            <div className="col-6">
                                <input className="search-bar" type="text" placeholder="Search Ideas" />
                            </div>
                            <div className="col-3">
                                <button className="button-style"><Link to="/myaccount">My Account</Link></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ideas-container">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-3">
                                <div className="filter-box">
                                    <div className="filter-topics-container">
                                        <div className="filter-topic"><input type="checkbox" id="1"/><label htmlFor="1">Engineering</label></div>
                                        <div className="filter-topic"><input type="checkbox" id="2"/><label htmlFor="2">Engineering</label></div>
                                        <div className="filter-topic"><input type="checkbox" id="3"/><label htmlFor="3">Engineering</label></div>
                                        <div className="filter-topic"><input type="checkbox" id="4"/><label htmlFor="4">Engineering</label></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6">
                                {
                                    (this.state.ideas_container || []).map((idea)=>{
                                        return <IdeaCard key={idea._id} {...idea} />
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MainIdeasDisplay;