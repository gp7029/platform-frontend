import Axios from 'axios';
const hostname = 'http://localhost:8080';
const api_calls = {
    async GET(uri, params={}) {
        try {
            return await Axios.get(`${hostname}${uri}`, {params: params});
        } catch (error) {
            alert(error.message);
        }
    },
    async POST(uri, payload={}) {
        try {
            return await Axios.post(`${hostname}${uri}`, payload);
        } catch (error) {
            alert(error.message);
        }
    },
    async DELETE(uri, payload={}){
        try {
            return await Axios.delete(`${hostname}${uri}`, { data: payload});
        } catch (error) {
            alert(error.message);
        }
    }
}

const getActiveLogin = () => {
    return {
        userid: 'gsingh',
        authtoken: 'authtoken'
    }
}

const isValidResponse = (response) => {
    return response && response.status ==  200 && response.data && response.data.payload;
}

const messageHandler = (ref, message, message_type) => {
    ref.setMessageState(message, message_type);
}

export {
    api_calls,
    getActiveLogin,
    isValidResponse,
    messageHandler
}

